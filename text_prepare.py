import string
import re

with open('J K Rowling - Harry Potter.txt', encoding='ascii', errors='ignore') as file:
    text = file.read()


text = text.casefold()
text = text.replace('\n', ' ')
text = ''.join(s for s in text if s in string.ascii_lowercase + ' ')
text = re.sub(r'p a g e.*?rowling', '', text, re.M)

while True:
    text_ = text.replace('  ', ' ')
    if text_ == text:
        break
    text = text_


print(text)
with open('Harry Potter - filtered.txt', 'w', encoding='ascii', errors='ignore') as file:
    file.write(text)